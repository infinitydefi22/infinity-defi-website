import React, { useState } from 'react';

import {Zoom, Fade} from '@albertlo/react-reveal';
import Image from 'next/image'

import info01 from '../public/info01.png'
import info02 from '../public/info02.png'
import info03 from '../public/info03.png'
import info04 from '../public/info04.png'
import info05 from '../public/info05.png'

import next from '../public/next.png'
import prev from '../public/prev.png'

function NftSalesInfo () {
  const maxSlide = 4
  const [current, setCurrent] = useState(1)

  const nextSlide = (e) => {
    console.log('nextSlide', maxSlide)
    e.preventDefault()
    if (current === maxSlide) {
      setCurrent(1)
    } else {
      setCurrent(current + 1)
    }
  }
  const prevSlide = (e) => {
    console.log('prevSlide', maxSlide)
    e.preventDefault()
    if (current === 1) {
      setCurrent(maxSlide)
    } else {
      setCurrent(current - 1)
    }
  }
  return (
    <>
      <div className='section NftSalesInfo' id='info'>
        
        <div className='blackBg'>
          <Zoom>
            <h1 className='title'>
              NFT Mint Info
            </h1>
          </Zoom>
          
          <Fade>
            <p className='text-center'>
              A completely new form of minting event
            </p>
          </Fade>
        </div>

        <div className='container'>
          <div className='slides'>
            <a className='nav prev' onClick={prevSlide}>
              <Image src={prev} layout="fill" objectFit="cover" /> 
            </a>
            <a className='nav next' onClick={nextSlide}>
              <Image src={next} layout="fill" objectFit="cover" /> 
            </a>

            <div className={current === 1 ? 'current slide' : 'slide'}>
              <h3>Total NFT mintable: 9.999</h3>
              <p>
                It will be possible to mint only 9.999 NFTs for this event.
              </p>
              <div className={current === 1 ? 'current image' : 'image'}>
                <Image src={info01} />
              </div>
            </div>

            <div className={current === 2 ? 'current slide' : 'slide'}>
              <h3>
                Minimum price 0.10 USDC
              </h3>
              <p>
                The starting price for each NFT is 0.10 USDC, 
                the price will only go up during the minting event.
              </p>
              <div className={current === 2 ? 'current image' : 'image'}>
                <Image src={info02} />
              </div>
            </div>

            <div className={current === 3 ? 'current slide' : 'slide'}>
              <h3>Maximum price 250 USDC </h3>
              <p>
                The maximum price for a single NFT is 250 USDC, the price can’t go down
              </p>
              <div className={current === 3 ? 'current image' : 'image'}>
                <Image src={info03} />
              </div>
            </div>

            <div className={current === 4 ? 'current slide' : 'slide'}>
              <h3>No ending date</h3>
              <p>
                The sale will continue without an end, 
                because the NFT will generate instantly reward.
              </p>
              <div className={current === 4 ? 'current image' : 'image'}>
                <Image src={info05} />
              </div>
            </div>

          </div>
        </div>
        
      </div>
    </>
  )
}
export default NftSalesInfo
