import React, { useState, useEffect,useRef } from 'react';
import ReactTypingEffect from 'react-typing-effect'

import Image from 'next/image'
import logo from '../public/logo.png'
import github from '../public/github.png'
import twitter from '../public/twitter.png'
import discord from '../public/discord.png'


function Hero () {
  const [top, setTop] = useState(false)
  const logoRef = useRef();
  useEffect(() => {
    const scrollHandler = () => {
      const logoX = logoRef.current.getBoundingClientRect().x
      window.pageYOffset >= logoX ? setTop(true) : setTop(false)
    }

    window.addEventListener('scroll', scrollHandler)
    scrollHandler()
    
    return () => {
      window.removeEventListener('scroll', scrollHandler)
    }
  }, [])
  return (
    <div className='Hero' id='hero'>
      <a className='help' target='_blank' href={process.env.NEXT_PUBLIC_DISCORD}>
        <div className='icon'><Image src={discord} /></div>  Chat with us!
      </a>
      {top
        ? <div className='Navbar'>
            <div className='logoNav'>
              <a href='#hero'>
                <Image src={logo} />
              </a>
            </div>
            <span className='links'>
              <a href='#what'>What is?</a>
              <a href='#benefit'>NFT Benefit</a>
              <a href='#mint'>Minting Event</a>
              <a href='#info'>Mint Info</a>
              <a href='#distribution'>Distribution</a>
              <a href='#faq'>FAQ</a>
              <a href='#roadmap'>Roadmap</a>
              <a href='#join'>Join us</a>
            </span>  
        </div>
        : null
      }
      <div className='a bg'>
        <div className='c'>
          <div className='cc'>
            <div className='logo' ref={logoRef} >
              <Image src={logo} />
            </div>
            
            <div className='container'>
              <div className='phraseTyping title'>
                is&nbsp;<ReactTypingEffect
                  text={['a DAO', 'a Minting Event', 'an NFT', 'a New Protocol', 'an AMM', 'a Treasury']}
                  speed={'180'}
                  eraseSpeed={'100'}
                  eraseDelay={'2100'}
                  typingDelay={'100'}
                  cursor={' '}
                  displayTextRenderer={(text, i) => {
                    return (<span className='typed-text'>{text}</span>)
                  }}
                /> <span className='cursor' /> 
                <br />
                for everyone to grow and prospere!
              </div>
            </div>
            <a href={process.env.NEXT_PUBLIC_GITHUB} target='_blank' className='s'>
              <Image src={github} /> 
            </a>
            <a href={process.env.NEXT_PUBLIC_TWITTER} target='_blank' className='s'>
              <Image src={twitter} /> 
            </a>
            <a href={process.env.NEXT_PUBLIC_DISCORD} target='_blank' className='s'>
              <Image src={discord} /> 
            </a>
          </div>
        </div>
        <div className='b' />
        <div className='bb' />
      </div>
    </div>
  )
}
export default Hero
