import { useState, useEffect} from 'react'
import Chart from "./Chart"

const data1 = [
  { name: 'Liquidity Pool', value: 150 },
  { name: 'DEV', value: 350 },
  { name: 'Treasury', value: 500 }
];
const data2 = [
  { name: 'Security Audit', value: 125 },
  { name: 'DEV', value: 750 },
  { name: 'Marketing', value: 125 }
];

const colors1 = ['#006CA4', '#02C4CC', '#0099BC']
const colors2 = ['#A1066E', '#FE5757', '#D71765']


function NftDistribution () {
  const [isVisible, setIsVisible] = useState(true);
  
  useEffect(() => {   
    window.addEventListener("scroll", listenToScroll);
    return () => 
       window.removeEventListener("scroll", listenToScroll); 
  }, [])
  
  const listenToScroll = () => {
    let heightToHideFrom = 2550

    const winScroll = document.body.scrollTop || 
        document.documentElement.scrollTop;

    if (winScroll < heightToHideFrom) {  
         isVisible && setIsVisible(false);
    } else{
         setIsVisible(true);
    }  
  };
  return (
    <>
      <div className='section NftDistribution' id='distribution'> 
        <div className='box'>
          <div className='purpleBg'>
            <div className='purpleAngleTop' />
            <h1 className='title'>
              NFT Minting
              <br />
              Distribution
            </h1>
            <div className='purpleAngleBottom' />
          </div>
          <div className='container'>
          {
          isVisible 
          && 
            <div className='row'>
              <div id="hide" className='col-md-6 d-flex flex-column justify-content-center align-items-center'>
                <h2>Global distribuition</h2>
                <Chart callData={data1} colors={colors1} />
              </div>
              <div id="hide" className='col-md-6 d-flex flex-column justify-content-center align-items-center'>
                <h2>DEV Distribuition</h2>
                <Chart callData={data2} colors={colors2} />
              </div>
            </div>
          }
          </div>
        </div>
      </div>
    </>
  )
}
export default NftDistribution
