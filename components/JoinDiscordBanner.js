import Image from 'next/image'
import discord from '../public/discord-b.png'

function JoinDiscordBanner () {
  return (
    <>
      
      <div className='JoinDiscordBanner' id='join'>
        <div className='container text-center'>
          <div className='discordBanner'>
            <h1 className='title'>
              Join now our 
              <br />
              Discord Server
            </h1>
            <p>
              And Join the conversation!
              </p>
            <a className='btn' href={process.env.NEXT_PUBLIC_DISCORD} target='_blank'>
              <div className='icon'><Image src={discord} /></div> Join Discord
            </a>
          </div>
        </div>
      </div>
    </>
  )
}
export default JoinDiscordBanner
