import Image from 'next/image'
import monts from '../public/monts.png'

import JoinDiscordBanner from '../components/JoinDiscordBanner'

function Footer () {
  return (
    <>
      
      <div className='section Footer'>
        <div className='monts'>
          <Image src={monts} />
        </div>
        <div className='container text-center'>
          <JoinDiscordBanner />
        </div>
      </div>
    </>
  )
}
export default Footer
