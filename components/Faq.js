import React, { useState } from 'react';
import FaqItem from './FaqItem'

import Image from 'next/image'
import faq from '../public/faq.png'

function Faq () {
  const faqs = [
    { 
      id: 1,
      title: 'What is infinity DeFi?',
      text: 'Infinity DeFi is a decentralized protocol that aims to reward everyone that want to participate in it, with a sustainable APY for long term vision.'
    },
    { 
      id: 2,
      title: 'How can i participate in it?',
      text: 'You can mint an NFT and be rewarded instantly.'
    },
    { 
      id: 3,
      title: 'On which chain?',
      text: 'We are on AVALANCHE(AVAX)'
    },
    { 
      id: 4,
      title: 'What are the benefit of hodler for this NFTs',
      text: '1. reward of x% in token, 2. reword of X% in stable coin, 3. power of voting on the DAO, 4. 30% discount on next minting events.'
    },
    { 
      id: 5,
      title: 'What token i need to mint the NFT?',
      text: 'AVAX, the base crypto of Avalanche blockchain'
    },
    { 
      id: 6,
      title: 'How to enter in the whitelist?',
      text: 'We don\'t have any whitelist.'
    },
    { 
      id: 7,
      title: 'How much is the cost for a NFT?',
      text: 'The cost is variable you can check it on our website in the minting section. It start from 0.10 USD and arrive to a max of 250 USD.'
    },
    { 
      id: 8,
      title: 'How much will be my reward?',
      text: 'The reward will be 1% per day in our Token and another part in stablecoin.'
    },
    { 
      id: 9,
      title: 'If I invite a friend in the server can I have a reward?',
      text: 'Yes! we will give you a discount on the NFT minting price'
    }
  ]  
  return (
    <>
      <div className='section Faq' id='faq'>
        <div className='container'>
          <div className='row align-items-center'>
            <div className='col-md-6 titleImage'>
              <Image src={faq} />
            </div>
            <div className='col-md-6 text-center'>
              <h1 className='title'>
                F.A.Q.
              </h1>
            </div>
          </div>
        </div>

        <div className='container'>
          
          {faqs.map((item)=>{
            return (<FaqItem item={item} />)
          })}
        </div>
        
      </div>
    </>
  )
}
export default Faq
