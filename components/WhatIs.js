// import { Fade } from 'react-awesome-reveal';
import {Zoom} from '@albertlo/react-reveal';

import {
  AppearingContainer,
  AppearSequentialContainer
} from 'react-appear-on-scroll'



import Image from 'next/image'

import infinity from '../public/Infinity.png'
import dinos from '../public/dinos.png'
import robot from '../public/robot.png'

function WhatIs () {
  return (
    <>
      <div className='section WhatIs' id='what'>
        
        <div className='blackBg'>
          <div className='blackAngleTop' />
          <Zoom>
            <h1 className='title'>
              What is infinity DeFi?
            </h1>
          </Zoom> 
          <div className='blackAngleBottom' />
        </div>
        

        <div className='container'>
          <div className='content'>
            <AppearingContainer
              animationType='scale'
              transitionType='bouncy'
              fading
              stayVisible
              delay={200}
            >
              <div className='row'>
                <div className='col-md-4'>
                  <div className='image'>
                    <Image src={infinity} />  
                  </div>
                  <p>
                    Infinity DeFi is a community driven financial 
                    protocol that aims to build a sustainable ecosystem 
                    for  every participant.
                  </p>
                </div>  
                <div className='col-md-4'>
                  <div className='image'>
                    <Image src={dinos} />
                    <p>
                      We want to create a trustless ecosystem to ​enhance 
                      transparency and growth from the team to our community 
                    </p>
                  </div>
                </div>  
                <div className='col-md-4'>
                  <div className='image'>
                    
                    <Image src={robot} />
                    <p>
                    All smart contracts will be governed by the DAO, 
                    every decision at any level is made only by the DAO, 
                    this ensures transparency for everyone. 
                    </p>
                  </div>
                </div>  
              </div>
            </AppearingContainer>
            
          </div>
        </div>

      </div>
    </>
  )
}
export default WhatIs
