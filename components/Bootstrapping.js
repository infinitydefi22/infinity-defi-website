import { ParallaxBanner, ParallaxProvider } from 'react-scroll-parallax'


const Bootstrapping = () => {
    return (
        <>
        <div className='bootstrapping' id='mint'>
            <div className='wall_top'>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
            <ParallaxProvider>
                <ParallaxBanner
                    id='parallax_bootstrapping'
                    className='bootstrapping'
                    layers={[
                        {
                        image:
                            'bg_rocket.png',
                        speed: -20,
                        expanded: false
                        },
                    ]}
                >
                <div className='text_w'>
                    <div className='text'>
                        <h1 className='title'>Bootstrapping event</h1>
                        <h3>
                            After minting you will instantly start earn stable coin
                        </h3>
                        <p>
                            During bootstrapping event will be possible to mint  NFT, those will  give to minter the opportunity to earn a passive income for life.
                        </p>
                        <p>
                            The minting price will increase in time, the reason for this mechanism is to enable different size of investment.
                        </p>
                        <p>
                            The part of reward from minting will be deposited in a treasury smart contract, this ensure transparency from day 1 and give the opportunity to the future DAO to have a starting capital.
                        </p>
                        
                        
                        <div className='d-flex align-items-center'>
                            <h1>
                                1% daily
                            </h1>
                            {/* <a>learn more</a> */}
                        </div>
                    </div> 
                </div>
                </ParallaxBanner>
            </ParallaxProvider>
            <div className='wall_bottom'>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        </>
    )
}

export default Bootstrapping