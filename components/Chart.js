import { useState, useEffect } from "react"
import { PieChart, Pie, Cell, ResponsiveContainer } from "recharts"


function Chart ({ callData, colors }) {
  const [data, setData] = useState([])

  useEffect(() => {
    setData(callData);
  }, [callData])

  const RenderLabelContent = ({ percent, x, y, midAngle, name }) => {
    return (
      <g
        transform={`translate(${x}, ${y})`}
        textAnchor={midAngle < -90 || midAngle >= 90 ? "end" : "start"}
      >
        <text fontSize='10px' fontWeight='bold' fontFamily='"Roboto", "Helvetica", "Arial", sans-serif' fill='#fff' x={0} y={-5}>{`${name}`}</text>
        <text fontSize='16px' fontWeight='bold' fontFamily='"Roboto", "Helvetica", "Arial", sans-serif' fill='#fff' x={0} y={15}>{`${(
          percent * 100
        )}%`}</text>
      </g>
    );
  };

  return (
    <ResponsiveContainer width={400} height={400}>
        <PieChart>
            <Pie
            data={data}
            dataKey='value'
            cy={200}
            cx={200}
            startAngle={180}
            endAngle={-180}
            innerRadius={50}
            outerRadius={100}
            label={RenderLabelContent}
            isAnimationActive={true}
            >
            {data.map((_, index) => (
                <Cell
                key={`slice-${index}`}
                strokeWidth={"3"}
                stroke={null}
                fill={colors[index]}
                />
            ))}
            </Pie>
        </PieChart>
    </ResponsiveContainer>
  )
}

export default Chart