import React, { useState } from 'react';

import Image from 'next/image'
import openImage from '../public/open.png'
import closeImage from '../public/close.png'

function FaqItem ({item}) {
  const [open, setOpen] = useState(false)
  const handleOpen = (e) => {
    e.preventDefault()
    setOpen(!open)
  }
  return (
    <div className='box-shadow-3d' key={item.id}>
      <div className='box' onClick={handleOpen}>
        <div className='nav'>
        <a onClick={handleOpen}>
          {open
            ? <Image src={closeImage} />
            : <Image src={openImage} />
          }
        </a> 
        </div>
        <h3>
          {item.title}
        </h3>
        <p className={open ? 'open' : null}>
          {item.text}
        </p>
      </div>
    </div>
  )
}
export default FaqItem
