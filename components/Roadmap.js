function Roadmap () {
  return (
    <>
      <div className='section Roadmap' id='roadmap'>
        <div className='container'>
        <h1 className='title text-center'>Roadmap</h1>

        </div>
        

        <div className='container'>
          <div className='main-timeline'>

                
                <div className='timeline'>
                    <div className='icon'></div>
                    <div className='date-content'>
                        <div className='date-outer'>
                            <span className='date'>
                              <span className='year'>Q2</span>
                              <span className='month'>Apr - Jun</span>        
                            </span>
                        </div>
                    </div>
                    <div className='timeline-content'>
                        <h5 className='title'>Bootstrapping</h5>
                        <p className='description'>
                            
                        - Publish documentation & NFT sale  <br />
                        - Marketing campaign launch  <br />
                        - NFT minting event 

                        </p>
                    </div>
                </div>
                
                <div className='timeline'>
                    <div className='icon'></div>
                    <div className='date-content'>
                        <div className='date-outer'>
                            <span className='date'>
                              <span className='year'>Q3</span>
                              <span className='month'>Jul-Sep</span>
                            </span>
                        </div>
                    </div>
                    <div className='timeline-content'>
                        <h5 className='title'>NFT Launchpad & Marketplace</h5>
                        <p className='description'>
                          - LaunchPlace (A launchPad and a Marketplace of NFT) <br />
                        </p>
                    </div>
                </div>

                <div className='timeline'>
                    <div className='icon'></div>
                    <div className='date-content'>
                        <div className='date-outer'>
                            <span className='date'>
                              <span className='year'>Q4</span>
                              <span className='month'>Oct-Dic</span>
                            </span>
                        </div>
                    </div>
                    <div className='timeline-content'>
                        <h5 className='title'>The DAO</h5>
                        <p className='description'>
                          - bootstrap dao event <br />
                          - initial dashboard dao <br />
                          - partner with influencer <br />
                          - release DAO
                        </p>
                    </div>
                </div>
                
                <div className='timeline'>
                    <div className='icon'></div>
                    <div className='date-content'>
                        <div className='date-outer'>
                            <span className='date'>
                              <span className='year'>Q1</span>
                              <span className='month'>Jan-Mar</span>
                            </span>
                        </div>
                    </div>
                    <div className='timeline-content'>
                        <h5 className='title'>The AMM</h5>
                        <p className='description'>
                          - bootstrap event AMM <br />
                          - testnet AMM <br />
                          - main AMM
                        </p>
                    </div>
                </div>
            </div>
          </div>
      </div>
    </>
  )
}
export default Roadmap
