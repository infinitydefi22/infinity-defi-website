import {Zoom} from '@albertlo/react-reveal'
import {
  AppearingContainer,
  AppearSequentialContainer
} from 'react-appear-on-scroll'



import Image from 'next/image'

// import vote from '../public/nft_ben_vote.png'
// import rev from '../public/nft_ben_rev.png'
// import earn from '../public/nft_ben_earn.png'
import earn from '../public/nft_ben_earn2.png'
import vip from '../public/nft_ben_vip.png'
import vote from '../public/nft_ben_lego.png'
import rev from '../public/nft_ben_passive.png'

function NftBenefit () {
    
  return (
        <div className='NftBenefit' id='benefit'>
            <div className='blackBg'>
              <div className='blackAngleTop' />
              <Zoom>
                <h1 className='title'>
                  Benefits of our NFTs
                </h1>
              </Zoom> 
              <div className='blackAngleBottom' />
            </div>

            <div className='container'>
              <div className='content'>
                <AppearingContainer
                  animationType='scale'
                  transitionType='bouncy'
                  fading
                  stayVisible
                  delay={200}
                >
                  <div className='row text-center'>
                    <div className='col-md-3'>
                      <div className='image'>
                        <Image src={vote} />  
                      </div>
                      <p>
                        Voting power in the DAO.
                      </p>
                    </div>  
                    <div className='col-md-3'>
                      <div className='image'>
                        <Image src={rev} />
                        <p>
                          Passive Income with revenue share for AMM.
                        </p>
                      </div>
                    </div>  
                    <div className='col-md-3'>
                      <div className='image'>
                        
                        <Image src={earn} />
                        <p>
                          Lock & Earn reword in project token and stable coin.
                        </p>
                      </div>
                    </div>  
                    <div className='col-md-3'>
                      <div className='image'>
                        
                        <Image src={vip} />
                        <p>
                          VIP membership for new upcoming bootstrap event.
                          <br />
                          30% discount on each new NFT minting.
                        </p>
                      </div>
                    </div>  
                  </div>
                </AppearingContainer>
                
              </div>
            </div>
        </div>
    )
}

export default NftBenefit