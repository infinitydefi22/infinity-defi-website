// import styles from '../styles/Home.module.css'
import MainContainer from '../components/MainContainer'

import Hero from '../components/Hero'
import WhatIs from '../components/WhatIs'
import NftBenefit from '../components/NftBenefit'
import Bootstrapping from '../components/Bootstrapping'
import NftSalesInfo from '../components/NftSalesInfo'
import NftDistribution from '../components/NftDistribution'
import Faq from '../components/Faq'
import Roadmap from '../components/Roadmap'
import Footer from '../components/Footer'

function Home () {
  return (
    <MainContainer title='Landing'>
      <Hero />
      <WhatIs />
      <NftBenefit />
      <Bootstrapping />
      <NftSalesInfo />
      <NftDistribution />
      <Faq />
      <Roadmap />
      <Footer />
    </MainContainer>
  )
}
export default Home
