import 'bootstrap/dist/css/bootstrap.css'

import '../styles/__globals.css'
import '../styles/Bootstrapping.css'
import '../styles/WhatIs.css'
import '../styles/NftBenefit.css'
import '../styles/NftSalesInfo.css'
import '../styles/NftDistribution.css'
import '../styles/Faq.css'
import '../styles/Roadmap.css'
import '../styles/Footer.css'
import '../styles/JoinDiscordBanner.css'
import '../styles/Dapp.css'

import '../styles/Range.css'

import 'react-appear-on-scroll/dist/index.css'

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
