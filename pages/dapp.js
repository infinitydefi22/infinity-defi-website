import React, {useState} from 'react'
import Image from 'next/image'
import logo from '../public/logo.png'
import metamask from '../public/MetaMask.png'
import greenGlass from '../public/green-glass.png'
import mint from '../public/mint-icon.png'

import info from '../public/info01.png'

import Range from '../components/Range'

import MainContainer from '../components/MainContainer'
function Dapp () {
    const [period, setPeriod] = useState(4)
    const [lock, setLock] = useState(true)

    const periods = [
        {time:'1 week', apy:'2% APY', id: 1},
        {time:'1 month', apy:'10% APY', id: 2},
        {time:'6 months', apy:'150% APY', id: 3},
        {time:'1 year', apy:'365% APY', id: 4}
    ]

    const changePeriod = (id) => {
        setPeriod(id)
    }
    
    const changeLock = (status) => {
        setLock(status)
    }
    
    return (
        <MainContainer title='DApp'>
        <div className='Dapp'>
            <div className='container-flex head'>
                <div className='row'>
                    <div className='col-6 p-3 left'>
                        <div className='logo-dapp'>
                            <Image src={logo} />
                        </div>
                    </div>
                    <div className='col-6 p-3 action'>
                        <button className='w-shadow position-relative btn-metamsk btn btn-primary'>Connect Metamask
                            <div className='icon-metamask'>
                                <Image src={metamask} />
                            </div>
                        </button>
                    </div>
                </div>
            </div>
            <div className='container-flex body'>
                <div className='row'>
                    <div className='col-md-4 p-3' />
                    <div className='col-md-8 p-3'>
                        <div className='row'>
                            <div className='col-md-7'>
                                <h3 className='text-center mint-title'>
                                    Mint the NFTs
                                </h3>
                                <div className='row position-relative'>
                                    <div className='btn-random'>
                                        <button className='w-shadow position-relative btn btn-primary'> Change Image
                                            <div className='icon-random'>
                                                <Image src={greenGlass} />
                                            </div>
                                        </button>
                                        <div>Click to get new random NFT</div>
                                    </div>
                                    <div className='bg-img-nft' />
                                    <div className='col-md-6' />
                                    <div className='col-md-6 '>
                                        <div className='nft-image'>
                                            <Image src={info} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className='col-md-5 d-flex flex-column justify-content-center align-items-center'>
                                <div className='d-flex flex-column justify-content-center align-items-center'>
                                    <span>qty</span>
                                    <h1>1</h1>
                                </div>
                                <div className='w-75 mb-4 mt-4'>
                                    <Range />
                                </div>
                                <div className='d-flex flex-column justify-content-center align-items-center'>
                                    <h1>$ 10.00</h1>
                                    <h3>TKN 3,3456</h3>
                                </div>
                            </div>
                        </div>   


                        {/*
                            Select Lock & Earn - START
                        */}
                        <div className='row p-2'>
                            <div className='col-md-6'>
                                <div onClick={(e) => {
                                    e.preventDefault()
                                    changeLock(true)
                                }} className={lock ? 'lockEarn selected w-shadow' : 'lockEarn w-shadow'}>
                                    <div className='label'>
                                        <div className='circle'><div className='circle2' /></div>
                                        <h3>Lock & Earn</h3>
                                        Select the lock time and APY
                                    </div>
                                    <div>
                                        {periods.map((item) => {
                                            return (
                                                <div className='period-box'>
                                                    <div onClick={(e) => {
                                                        e.preventDefault()
                                                        changePeriod(item.id)
                                                    }} className={period === item.id ? 'period selected w-shadow' : 'period w-shadow'}>
                                                        <div className='period-circle' />
                                                        <div className='time'>{item.time}</div>
                                                        <div className='apy'>{item.apy}</div>
                                                    </div>
                                                </div>
                                            )
                                        })}
                                    </div>
                                </div>
                            </div>
                            <div className='col-md-6'>
                                <div onClick={(e) => {
                                    e.preventDefault()
                                    changeLock(false)
                                }}  className={!lock ? 'lockEarn selected w-shadow' : 'lockEarn w-shadow'}>
                                <div className='label'>
                                        <div className='circle'><div className='circle2' /></div>
                                        Don’t stake now I’m not interested in APY right now. Just mint the NFT
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/*
                            Select Lock & Earn - START
                        */}     
                        <div className='row'>
                            <div className='col-12'>
                                <button className='w-shadow position-relative btn-mint btn btn-primary'>Mint Now!
                                    <div className='icon-mint'>
                                        <Image src={mint} />
                                    </div>
                                </button>             
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </MainContainer>
    )
}

export default Dapp